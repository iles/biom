Cufon.replace('.rp'); // Requires a selector engine for IE 6-7, see above
$('nav ul li.submenu').on('mouseover', function(){
	$('.sub-menu').fadeIn();
	$('.menu').hide();
	$('#'+$(this).attr('menu')).fadeIn();

})
$('nav ul li.nomenu, .main-section').on('mouseover', function(){
	$('.sub-menu').fadeOut();
})

 $(document).ready(function(){
    // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
	    $('.modal-trigger').leanModal();

		 var options = { 
		    success: showResponse  
		}; 

		function showResponse(responseText, statusText, xhr, $form)  { 
			    $('.basket-list').html(responseText.items);
			    $('.total-amount').html(responseText.total);
		}  
		// pass options to ajaxForm 
		$('.basketForm').ajaxForm(options);

		$('.cross').on('click', function(event){
			event.preventDefault();
			var id = $(this).attr('data-id');
			$.ajax({
				url: $(this).attr('href'),
				data: { id: id },
				success: function(data){
					$('.item[data="'+id+'"]').fadeOut();
					$('.total-amount').html(data);
				 }
			});
		})		

		$('.basketForm input').on('focus', function(event){
			$('.prod-part .main-prod').css('overflow', 'hidden');
		})		

		$('.basketForm input').on('blur', function(event){
			$('.prod-part .main-prod').css('overflow', 'auto');
		})





  });



