<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\touchspin\TouchSpin;
?>


<div class="prod-single">
    <div class="leftpart">
        <img src="uploads/product/<?= $model->id; ?>/<?= $model->image; ?>">
    </div>
    <div class="rightpart">
        <p class="title rp"><?= $model->title; ?></p>
        <p class="price rp"><?= $model->price; ?> тг/кг</p>
        <p class="amount rp">Какое кол-во желаете?</p>
        <form class="basketForm">
            <input type="number" min="1" max="30" name="amount" />
            <input type="hidden" value="<?= $model->id; ?>" name="id">
            <div class="button-wrap">
                <button class="waves-effect waves-green btn rp" type="submit">В корзину</button>
            </div>
            <div class="button-wrap">   
                <a href="#" class="waves-effect waves-green btn rp">Оформить заказ</a>
            </div> 
        </form>
        <p class="desc"><?= $model->desc; ?></p>    
    </div>
</div>