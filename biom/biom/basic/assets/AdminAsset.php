<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AdminAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/materialize.css',
        'css/admin.css',
    ];
    public $js = [
        'js/vendor/jquery-1.11.3.min.js',
        'js/vendor/modernizr-2.8.3.min.js',
        'js/vendor/materialize.min.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
