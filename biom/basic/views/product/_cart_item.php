<?php
	use yii\helpers\Html;
	use yii\helpers\Url;
?>                      <div class="item" data="<?= $position->id; ?>">
                            <div class="leftpart">
                                <p class="title rp"><?= $position->title; ?></p>
                                <p class="price rp"><?= $position->quantity?>кг / <?= $position->price ?> тг</p>
                            </div>
                            <?= Html::a('<img src="img/cross.jpg">', Url::toRoute('product/remove'), ['class'=>'cross', 'data-id'=> $position->id] ); ?>
                            <div class="clearfix"></div>
                        </div> 