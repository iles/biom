<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ListView;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\Profile $profile
 */

$this->title = Yii::t('user', 'Мои заказы');
?>

        <div class="prod-part">
            <div class="head">
                <div class="info">
                    <p class="sub-category rp basket">Мои заказы</p>
                </div>
                <div class="main-head">
                    <?php if (!\Yii::$app->user->isGuest)  :?>
                        <?= Html::a('Профиль', Url::toRoute('user/settings/profile'))?>
                        <?= Html::a('Выйти', Url::toRoute('user/security/logout'), ['data-method' => 'post'])?>                
                    <?php else : ?>
                        <a href="#log_modal" class="modal-trigger">Войти</a>
                        <a href="#reg_modal" class="modal-trigger">Регистрация</a>
                    <?php endif; ?>
                </div>
                <div class="arrows">
                    <div class="left">
                        &#9668;
                    </div>
                    <div class="right">
                        &#9658;
                    </div>                      
                </div>
            </div>
            <div class="main-profile my-orders main-section">
                <div class="top-bar rp">Актуальные заказы</div>

                <?= ListView::widget( [
                        'layout'=>'{items}',
                        'dataProvider' => $dataProvider,
                        'itemView' => '_order_active',
                     ] ); 
                ?>



                <div class="top-bar closed rp">Закрытые заказы</div>
                <?= ListView::widget( [
                        'layout'=>'{items}',
                        'dataProvider' => $dataProvider,
                        'itemView' => '_order_closed',
                     ] ); 
                ?>
            
            </div>
        </div>
