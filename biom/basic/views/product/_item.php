<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use kartik\touchspin\TouchSpin;
?>


<div class="prod-single">
    <div class="leftpart">
        <img src="uploads/product/<?= $model->id; ?>/<?= $model->image; ?>">
    </div>
    <div class="rightpart">
        <p class="title rp"><?= $model->title; ?></p>
        <p class="price rp"><?= $model->price; ?> тг/кг</p>
        <p class="amount rp">Какое кол-во желаете?</p>
        <form class="basketForm" data-id="<?= $model->id; ?>">
            <input type="number" min="1" max="30" name="amount" value="1" />
            <input type="hidden" value="<?= $model->id; ?>" name="id">
            <div class="button-wrap">
                <button class="waves-effect waves-green btn rp" type="submit">В корзину</button>
            </div>
            <div class="button-wrap">   
                <a href="#" data-id="<?= $model->id; ?>" class="waves-effect waves-green btn rp ">Оформить заказ</a>
            </div> 
        </form>

    <?php $form = ActiveForm::begin([
        'method'=>'GET',
        'action'=>Url::toRoute('order/create'),
        'options'=>['class'=>'create-order-form', 'id'=>"form-".$model->id]
    ]); ?> 
            <input type="number" min="1" max="30" name="amount" value="1" />
            <input type="hidden" value="<?= $model->id; ?>" name="id">
    <?php ActiveForm::end(); ?>


    </div>
    <div class="bottom-part">
        <div class="left"></div>
        <div class="right">
            <p class="desc"><?= $model->desc; ?></p> 
        </div>
    </div>
</div>