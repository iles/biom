<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav; 
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AdminAsset;

AdminAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <header>
      <div class="container"><a href="#" data-activates="nav-mobile" class="button-collapse top-nav waves-effect waves-light circle hide-on-large-only"><i class="mdi-navigation-menu"></i></a></div>
      <ul id="nav-mobile" class="side-nav fixed">
        <li class="logo">
          <a id="logo-container" href="/admin" class="brand-logo">  
            <img src="img/logo-small.png">
          </a>
        </li> 
        <li class="no-padding">
          <ul class="collapsible collapsible-accordion">
            <li class="bold" >
              <a class="collapsible-header waves-effect waves-teal" style="padding: 20px 10px;">Продукция</a>
              <div class="collapsible-body">
                <ul>
                  <li><?= Html::a('Вся продукция', ['product/index']); ?></li>
                  <li><?= Html::a('Добавить', ['product/create']); ?></li>
                </ul>
              </div>
            </li>
          </ul>
        </li>
        <li>
          <?= Html::a('Категории', ['category/index']); ?>
        </li>
      </ul>
    </header>
    <main>
<?= $content ?>
    </main>    


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>