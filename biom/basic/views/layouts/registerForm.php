<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use dektrium\user\models\RegistrationForm;
use dektrium\user\models\LoginForm;
use yii\helpers\Url;

/**
 * @var yii\web\View              $this
 * @var dektrium\user\models\User $user
 * @var dektrium\user\Module      $module
 */
$model = \Yii::createObject(RegistrationForm::className());
$modelLog = \Yii::createObject(LoginForm::className());
?>

<?php if(\Yii::$app->user->isGuest ) :?>

        <div id="reg_modal" class="modal user-modal">
           <div class="main-frame">
                <img src="img/logo-small.png">
                <p class="rp">Регистрация</p>
                <?php $form = ActiveForm::begin([
                    'id'                     => 'registration-form',
                    'enableAjaxValidation'   => true,
                    'enableClientValidation' => false,
                    'action' => Url::toRoute('user/registration/register'),
                    'options'=>['autocomplete'=>'off'],
                ]); ?>
                    <?= $form->field($model, 'email')->textInput(['autocomplete'=>'off', 'placeholder'=>'Ваш email', ])->label(false); ?>
                    <?= $form->field($model, 'password')->passwordInput(['autocomplete'=>'off', 'placeholder'=>'Пароль'])->label(false); ?>
                    <?= $form->field($model, 'agree', ['template'=>'{input}<label for="register-form-agree" class="rp">Согласен с <a href="">Правилами ресурса</a></label>'])->checkBox(['label' => null]); ?>
                    <?= Html::submitButton('Зарегистрировать', ['class' => 'waves-effect waves-green btn rp']) ?>
                <?php ActiveForm::end(); ?>
           </div>
        </div>        

        <div id="log_modal" class="modal user-modal">
           <div class="main-frame">
                <img src="img/logo-small.png">
                <p class="rp">Вход в профиль</p>
                <?php $form = ActiveForm::begin([
                    'id'                     => 'login-form',
                    'enableAjaxValidation'   => true,
                    'enableClientValidation' => false,
                    'validateOnBlur'         => false,
                    'validateOnType'         => false,
                    'validateOnChange'       => false,
                    'action' => Url::toRoute('user/security/login'),
                ]); ?>
                <?= $form->errorSummary($model); ?>
                    <?= $form->field($modelLog, 'login', ['inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1']])->label(false); ?>
                    <?= $form->field($modelLog, 'password', ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2']])->passwordInput()->label(false); ?>
                    <?= $form->field($modelLog, 'rememberMe', ['template'=>'{input}<label for="login-form-rememberme" class="rp">Запомнить</label>'])->checkBox(['tabindex' => '4', 'label' => null]); ?>
                    <?= Html::submitButton('Войти', ['class' => 'waves-effect waves-green btn rp', 'tabindex' => '3']) ?>
                <?php ActiveForm::end(); ?>
           </div>
        </div>
<?php endif; ?>




