<?php
    use yii\widgets\ListView;
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;
?> 
        <div class="prod-part">
            <div class="head">
                <img src="img/prod-icon1.png">
                <div class="info">
                    <p class="sub-category rp">Говядина</p>
                    <p class="category rp">Мясная продукция</p>
                </div>
            <div class="main-head">
            <?php if (!\Yii::$app->user->isGuest)  :?>
                <?= Html::a('Профиль', Url::toRoute('user/settings/profile'))?>
                <?= Html::a('Выйти', Url::toRoute('user/security/logout'), ['data-method' => 'post'])?>                
            <?php else : ?>
                <a href="#log_modal" class="modal-trigger">Войти</a>
                <a href="#reg_modal" class="modal-trigger">Регистрация</a>
            <?php endif; ?>
            </div>
                <div class="arrows">
                    <div class="left">
                        &#9668;
                    </div>
                    <div class="right">
                        &#9658;
                    </div>                   
                    
                </div>
            </div>
            <div class="main-prod main-section">

                <section class="basket-block">
                    <div class="top">
                        <img src="img/basket-logo-small.png">
                        <span class="rp">Ваша корзина</span>
                    </div>
                    <div class="basket-list">   
                        <?= $this->render('cart_block'); ?>
                    </div>
                    <div class="bottom">
                        <p class="total rp">Итого на сумму</p>
                        <p class="price rp "><span class="total-amount"><?= \Yii::$app->cart->getCost(); ?></span> тг</p>
                        <p class="total rp">Способ доставки</p>
                            <?php $form = ActiveForm::begin([
                                'method'=>'get',
                                'action'=> Url::toRoute('order/create'),
                            ]); ?>
                             <div class="input-wrap">
                                <input type="radio" id="del_1" checked="true" name="delivery" value="1">
                                <label for="del_1" class="rp" >City 1000</label>    
                            </div>                            
                            <div class="input-wrap">
                                <input type="radio" id="del_2" name="delivery" value="2">
                                <label for="del_2" class="rp">Express</label>   
                            </div>       
                <?= Html::submitButton('Оформить заказ', ['class' => 'm_button rp']) ?>                


    <?php ActiveForm::end(); ?>

                        
                    </div>
                </section>

                <?= ListView::widget( [
                        'layout'=>'{items}',
                        'dataProvider' => $dataProvider,
                        'itemView' => '_item',
                     ] ); 
                ?>

               

        </div>