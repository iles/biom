<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\CategorySearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Заказы';
?>
<div class="category-index">

    <h1><?= Html::encode($this->title) ?></h1>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'attribute' => 'id',
                'value' => function($model) {
                    return 'BIOMN'.str_pad($model->id, 7, '0', STR_PAD_LEFT); 
                },
            ],
            'status',
            'customers_firstname',
            'customers_lastname',
            'customers_email',
            'due_amount',
            [
                'attribute' => 'created_at',
                'format' => ['date', 'php:d.m.Y']
            ], 
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
