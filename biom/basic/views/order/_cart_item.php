                    <div class="item">
                        <div class="image">
                            <?= $position->getImage(); ?>
                        </div>
                        <div class="desc">
                            <p class="title rp"><?= $position->title; ?></p>
                            <p class="price rp"><?= $position->price; ?> тг/кг</p>
                            <div class="amount">
                                <p class="rp">Какое количество желаете?</p>
                                <input type="number" min="1" max="30" name="amount" value="<?= $position->quantity; ?>" />
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>