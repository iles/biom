-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Авг 27 2015 г., 11:53
-- Версия сервера: 5.6.19-0ubuntu0.14.04.1
-- Версия PHP: 5.6.10-1+deb.sury.org~trusty+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `bm_db`
--

-- --------------------------------------------------------

--
-- Структура таблицы `category`
--

CREATE TABLE IF NOT EXISTS `category` (
  `id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `category`
--

INSERT INTO `category` (`id`, `name`) VALUES
(1, 'Мясная продукция'),
(2, 'рыбная продукция'),
(3, 'Мясо птицы'),
(4, 'Молочная продукция'),
(5, 'Яйца'),
(6, 'Овощи');

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE IF NOT EXISTS `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m140209_132017_init', 1440394045),
('m140403_174025_create_account_table', 1440394045),
('m140504_113157_update_tables', 1440394045),
('m140504_130429_create_token_table', 1440394045),
('m140830_171933_fix_ip_field', 1440394045),
('m140830_172703_change_account_table_name', 1440394045),
('m141222_110026_update_ip_field', 1440394045);

-- --------------------------------------------------------

--
-- Структура таблицы `order`
--

CREATE TABLE IF NOT EXISTS `order` (
  `id` int(11) NOT NULL,
  `status` int(11) NOT NULL,
  `serial_number` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `customers_firstname` varchar(50) NOT NULL,
  `customers_lastname` varchar(50) NOT NULL,
  `customers_email` varchar(50) NOT NULL,
  `customers_contact_phone` varchar(50) NOT NULL,
  `customers_mobile_phone` varchar(50) NOT NULL,
  `order_city` varchar(50) NOT NULL,
  `order_street` varchar(50) NOT NULL,
  `order_house` varchar(50) NOT NULL,
  `order_apartment` varchar(50) NOT NULL,
  `order_location` varchar(70) NOT NULL,
  `notyfication_method` int(11) NOT NULL,
  `payment_method` int(11) NOT NULL,
  `delivery_method` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `order`
--

INSERT INTO `order` (`id`, `status`, `serial_number`, `user_id`, `customers_firstname`, `customers_lastname`, `customers_email`, `customers_contact_phone`, `customers_mobile_phone`, `order_city`, `order_street`, `order_house`, `order_apartment`, `order_location`, `notyfication_method`, `payment_method`, `delivery_method`) VALUES
(1, 0, 0, 0, 'Iles', 'saryyev', 'iless@mail.ru', '+77024611929', '+77024611929', 'Almaty', 'Karasay-batyr ', '209 ', '123', '', 0, 0, 0),
(2, 0, 0, 7, 'iles', 'saryyev', 'iless@mail.ru', '+7 707 819 69 45', '+7 707 819 69 45', 'Алматы', 'Джандосова', '45а', '8', '', 0, 2, 2);

-- --------------------------------------------------------

--
-- Структура таблицы `product`
--

CREATE TABLE IF NOT EXISTS `product` (
  `id` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `sub_category` int(11) DEFAULT NULL,
  `title` varchar(110) NOT NULL,
  `price` int(11) NOT NULL,
  `desc` varchar(500) NOT NULL,
  `image` varchar(80) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `product`
--

INSERT INTO `product` (`id`, `category`, `sub_category`, `title`, `price`, `desc`, `image`) VALUES
(6, 1, 1, 'Грудинка', 1700, 'Скажу честно: эта грудинка долгое время не давала мне покоя... а все дело в том, что однажды на рынке один дядечка, выбирая грудинку, так сочно рассказывал продавцу, что он собирается с ней сделать', 'meat2.jpg'),
(7, 1, 1, 'Ребрышки', 950, 'Если бы кто-нибудь раньше мне сказал, что мясо получится нежным всего за 20 минут, я бы не поверила, а уже сегодня я расскажу, как приготовить ребрышки в мультиварке', 'meat1.jpg'),
(8, 1, 2, 'Ребрышки', 1200, 'Конина отличается специфическим вкусом и является обычным и любимым блюдом у кочевых народов. Конина всегда была важной частью (иногда и ключевой) рациона кочевых тюркских и монгольских народов Азии (особенно ценится варёная колбаса казы-карта и чучук), также как и скисшее молоко лошадей — кумыс.', 'frenched_lamb_rack.jpg'),
(9, 1, 2, 'Нога', 2500, 'Известно также, что конская колбаса — это деликатес. Миф о мерзком вкусе конины, распространённый среди европейцев, возможно, связан с тем, что во время отступления из Москвы солдаты Наполеона ели павших лошадей, используя вместо соли и приправ порох, что вызвало многочисленные пищевые отравления.', 'images.jpg'),
(10, 1, 3, 'Конина', 2600, 'Конина употребляется при изготовлении некоторых сортов колбас для придания некоторой вязкости и упругости, а также пикантного привкуса.', 'ko.jpg'),
(11, 1, 3, 'Мясо', 6000, 'В Германии и во Франции конское мясо добавляют в копчёные колбасы.\r\nВ Туркмении запрещено употреблять конину в пищу', 'ko2.jpg');

-- --------------------------------------------------------

--
-- Структура таблицы `product_images`
--

CREATE TABLE IF NOT EXISTS `product_images` (
  `product_id` int(11) NOT NULL,
  `image` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `profile`
--

CREATE TABLE IF NOT EXISTS `profile` (
  `user_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `last_name` varchar(256) NOT NULL,
  `public_email` varchar(255) DEFAULT NULL,
  `mobile_phone` varchar(62) DEFAULT NULL,
  `contact_phone` varchar(62) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `avatar` varchar(70) NOT NULL,
  `city` varchar(70) NOT NULL,
  `street` varchar(120) NOT NULL,
  `house` varchar(15) NOT NULL,
  `apartament` varchar(15) NOT NULL,
  `sms_notify` int(11) NOT NULL,
  `email_notify` int(11) NOT NULL,
  `website` varchar(255) DEFAULT NULL,
  `bio` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `profile`
--

INSERT INTO `profile` (`user_id`, `name`, `last_name`, `public_email`, `mobile_phone`, `contact_phone`, `location`, `avatar`, `city`, `street`, `house`, `apartament`, `sms_notify`, `email_notify`, `website`, `bio`) VALUES
(6, NULL, '', NULL, 'iless@mail.ru', 'f6e2dd7e87120ad14e55c514ec600433', NULL, '', '', '', '', '', 0, 0, NULL, NULL),
(7, 'iles', 'saryyev', 'iless@mail.ru', '+7 707 819 69 45', '+7 707 819 69 45', 'dwad', '', 'Алматы', 'Джандосова', '45а', '8', 0, 0, '', ''),
(8, NULL, '', NULL, 'ilessss@mail.ru', '57e1c0ef760f30090a9704427da1ab6c', NULL, '', '', '', '', '', 0, 0, NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `social_account`
--

CREATE TABLE IF NOT EXISTS `social_account` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `provider` varchar(255) NOT NULL,
  `client_id` varchar(255) NOT NULL,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `token`
--

CREATE TABLE IF NOT EXISTS `token` (
  `user_id` int(11) NOT NULL,
  `code` varchar(32) NOT NULL,
  `created_at` int(11) NOT NULL,
  `type` smallint(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `username` varchar(25) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password_hash` varchar(60) NOT NULL,
  `auth_key` varchar(32) NOT NULL,
  `confirmed_at` int(11) DEFAULT NULL,
  `unconfirmed_email` varchar(255) DEFAULT NULL,
  `blocked_at` int(11) DEFAULT NULL,
  `registration_ip` varchar(45) DEFAULT NULL,
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `flags` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `email`, `password_hash`, `auth_key`, `confirmed_at`, `unconfirmed_email`, `blocked_at`, `registration_ip`, `created_at`, `updated_at`, `flags`) VALUES
(6, '', 'iless@mail.ru', '$2y$10$WYmbieT.hyii5qh7kvEIeOD0VMXHvmPQAaf.1ML1UITAEdU0HwMOO', 'lRHTiaJbpMNWavFEe5eW046w9Yz_dfmI', 1440416430, NULL, NULL, '192.168.10.1', 1440416430, 1440416430, 0),
(7, '', 'ilesss@mail.ru', '$2y$10$ODI8zHj1kgrNLtuOdvuP/O2IKO57blO4lMdZJBhOs3JPGxOM3mEZe', '9eoLDYTAvkY9IeHJaA1_9wj7AMH_WUao', 1440577062, NULL, NULL, '192.168.10.1', 1440577062, 1440577062, 0),
(8, '', 'ilessss@mail.ru', '$2y$10$wu9k51vGptgNxYXHrwVpwO3ZIw6KxmdD/miJo/z2aYX0Pnr6IkZxu', 'LjyeLLtm7K-aI_yIDd6atabTXQ6u4766', 1440580570, NULL, NULL, '192.168.10.1', 1440580570, 1440580570, 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `product_images`
--
ALTER TABLE `product_images`
  ADD KEY `product_id` (`product_id`);

--
-- Индексы таблицы `profile`
--
ALTER TABLE `profile`
  ADD PRIMARY KEY (`user_id`);

--
-- Индексы таблицы `social_account`
--
ALTER TABLE `social_account`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `account_unique` (`provider`,`client_id`),
  ADD KEY `fk_user_account` (`user_id`);

--
-- Индексы таблицы `token`
--
ALTER TABLE `token`
  ADD UNIQUE KEY `token_unique` (`user_id`,`code`,`type`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_unique_email` (`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `category`
--
ALTER TABLE `category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `order`
--
ALTER TABLE `order`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `product`
--
ALTER TABLE `product`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `social_account`
--
ALTER TABLE `social_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `profile`
--
ALTER TABLE `profile`
  ADD CONSTRAINT `fk_user_profile` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `social_account`
--
ALTER TABLE `social_account`
  ADD CONSTRAINT `fk_user_account` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

--
-- Ограничения внешнего ключа таблицы `token`
--
ALTER TABLE `token`
  ADD CONSTRAINT `fk_user_token` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
