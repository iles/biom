<?php

namespace app\models;

use Yii;
use yz\shoppingcart\CartPositionInterface;
use yz\shoppingcart\CartPositionTrait;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property integer $category
 * @property integer $sub_category
 * @property string $title
 * @property integer $price
 * @property string $desc
 * @property string $image
 *
 * @property ProductImages $id0
 */
class Product extends \yii\db\ActiveRecord implements CartPositionInterface
{
    use CartPositionTrait;

     public function getPrice()
    {
        return $this->price;
    }

    public function getId()
    {
        return $this->id;
    }

    public $imageFiles;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['category', 'title', 'price', 'desc'], 'required'],
            [['category', 'sub_category', 'price'], 'integer'],
            [['title'], 'string', 'max' => 110],
            [['desc'], 'string', 'max' => 500],
            [['image'], 'default'],
            [['imageFiles'], 'file', 'skipOnEmpty' => false, 'extensions' => 'png, jpg, jpeg', 'maxFiles' => 4],
        ];
    }





    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            
            foreach ($this->imageFiles as $file) {
                $names[] = $file->baseName . '.' . $file->extension;
            }
            
            $this->image = implode(';', $names);

            return true;
        } else {
            return false;
        }
    }    

    public function afterSave($insert, $changedAttributes)
    {
         $folder = Yii::getAlias('@webroot/uploads/').'/product/'.$this->id.'/';
            if (is_dir($folder) == false)
            mkdir($folder, 0777, true);
         
        foreach ($this->imageFiles as $file) {
            $file->saveAs($folder . $file->baseName . '.' . $file->extension);
        }

    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category' => 'Категория',
            'sub_category' => 'Подкатегрия',
            'title' => 'Название',
            'price' => 'Цена',
            'desc' => 'Описание',
            'image' => 'Изображение',
        ];
    }


}
