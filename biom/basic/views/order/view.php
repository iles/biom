<?php

/*
 * This file is part of the Dektrium project.
 *
 * (c) Dektrium project <http://github.com/dektrium>
 *
 * For the full copyright and license information, please view the LICENSE.md
 * file that was distributed with this source code.
 */

use yii\helpers\Html;
use yii\widgets\ListView;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var dektrium\user\models\Profile $profile
 */

$this->title = Yii::t('user', 'Мои заказы');
?>

        <div class="prod-part">
            <div class="head">
                <div class="info">
                    <p class="sub-category rp basket">Мои заказы</p>
                </div>
                <div class="main-head">
                    <a href="#">Войти</a>
                    <a href="#">Регистрация</a>
                </div>
                <div class="arrows">
                    <div class="left">
                        &#9668;
                    </div>
                    <div class="right">
                        &#9658;
                    </div>                      
                </div>
            </div>
            <div class="main-profile my-orders view main-section">
                <p class="date"><?= date('d.m.Y', $model->created_at); ?></p>
                <div class="order-bar">
                    <dl class="serial">
                    <dt class="rp">Серийный номер</dt>
                    <dd class="rp">BIOMN<?= str_pad($model->id, 7, '0', STR_PAD_LEFT); ?></dd>
                    </dl>                        
                    <dl class="status">
                    <dt class="rp">Статус заказа</dt>
                    <dd class="rp"><img src="img/green-bar.png">
                        <?php 
                            if($model->status == 1){
                                echo 'Ожидает обработки';
                            } elseif ($model->status == 2) {
                                echo 'Ожидает доставки';
                            } elseif ($model->status == 3) {
                                echo 'Заказ закрыт';
                            }
                        ?>
                    </dd>
                    </dl>   
                    <?= Html::a('Вернуться назад', ['order/orders'], ['class'=>'waves-effect waves-green btn rp']) ;?>
                    <div class="clearfix"></div> 
                </div>

                <div class="order-info">
                    <div class="date-block">
                        <p class="rp">Дата и время</p>
                        <p class="rp value">
                            <?php 
                                $monthes = array(
                                    1 => 'Января', 2 => 'Февраля', 3 => 'Марта', 4 => 'Апреля',
                                    5 => 'Мая', 6 => 'Июня', 7 => 'Июля', 8 => 'Августа',
                                    9 => 'Сентября', 10 => 'Октября', 11 => 'Ноября', 12 => 'Декабря'
                                );
                                echo(date('d ') . $monthes[(date('n'))] .' '. date('Y'));
                            ?>
                        </p>
                    </div>
                    
                    <div class="row order-view-top">
                        <div class="col s4 move">
                            <p>Список товаров</p>
                        </div>                        
                        <div class="col s3">
                            <p>Итого на сумму</p>
                        </div>                        
                        <div class="col s5">
                            <p>Данные заказчика</p>
                        </div>
                    </div>                    
                    <div class="row">
                        <div class="col s4">

                            <?php foreach($items as $item): ?>
                                <div class="order-item">
                                    <div class="image">
                                        <img src="uploads/product/<?= $item['id']; ?>/<?= $item['image'];?>">
                                    </div>
                                    <div class="info">
                                        <p class="rp"><?= $item['title']; ?></p>
                                        <p class="rp price"><?= $item['quantity']; ?> кг / <?= $item['quantity']; ?> тг.</p>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                            <?php endforeach; ?>
                        </div>   

                        <div class="col s3">
                            <p class="total_amount rp"><?= $model->due_amount; ?> т.</p>
                            <p>Способ доставки</p>
                            <input id="delivery_method" type="checkbox" checked disabled>
                            <label for="delivery_method" class="rp">
                               <?php if($model->delivery_method == 1){
                                    echo "City 1000";
                               } else {
                                    echo  "Express";
                               }

                               ?> 
                            </label>

                        </div> 

                        <div class="col s5">
                            <p><?= $model->customers_firstname; ?> , <?= $model->customers_lastname; ?></p>
                            <p><?= $model->customers_email; ?></p>
                            <p><?= $model->customers_contact_phone; ?></p>
                            <p><?= $model->customers_mobile_phone; ?></p>
                            <p><?= $model->order_city; ?></p>
                            <p><?= $model->order_street; ?></p>
                            <p><?= $model->order_house; ?></p>
                            <p><?= $model->order_apartment; ?></p>
                        </div>                     

                    </div>

                 
                </div>



             </div>
