<?php

use yii\db\Schema;
use yii\db\Migration;

class m150820_104051_create_product_table extends Migration
{
    public function up()
    {
        $this->createTable('product', [
            'id' => $this->primaryKey(),
            'title' => $this->string(),
            'desc' => $this->text(),
        ]);
    }

    public function down()
    {
        $this->dropTable('product')
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
