<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav; 
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

        <div id="reg_modal" class="modal">
           <div class="main-frame">
                <img src="img/logo-small.png">
                <p>Регистрация</p>
                <form>
                    <input type="text" placeholder="Ваш email">
                    <input type="password" placeholder="Пароль">
                    <input type ='checkbox' id="agree"><label for="agree">Я согласен с <a href="">Правилами ресурса</a></label>
                    <a href="#" class="waves-effect waves-green btn rp">Зарегистрировать</a>
                </form>
           </div>
        </div>

        <?= $this->render('menu'); ?>


         <?= $content ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
