<?php

namespace app\controllers;

use Yii;
use app\models\Order;
use app\models\Product;
use app\models\searchOrder;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * OrderController implements the CRUD actions for Order model.
 */
class OrderController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Order models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new searchOrder();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }    

    public function actionBasket()
    {
        $searchModel = new searchOrder();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('basket', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Order model.
     * @param integer $id
     * @return mixed
     */
    public function actionOverView($id)
    {
        return $this->render('overview', [
            'model' => $this->findModel($id),
        ]);
    }    

    public function actionView($id)
    {
        $model = $this->findModel($id);
        $items = $this->getItems($model->items);

        return $this->render('view', [
            'model' => $model,
            'items' => $items,
        ]);
    }

    public function getItems($ids){
        $datas = explode(';', $ids);
        $items = [];
        foreach ($datas as $data) {
            $item = [];
            $keys = explode('-', $data);
            $model = Product::find()->where(['id' => $keys[0]])->one();
            $item['id'] = $model->id;
            $item['title'] = $model->title;
            $item['price'] = $model->price;
            $item['image'] = $model->image;
            $item['quantity'] = $keys[1];
            $items[]  = $item;
        }
        return $items;
    }

    /**
     * Creates a new Order model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Order();

        if( !\Yii::$app->user->isGuest ){
            $profile = \Yii::$app->user->identity->profile;
            $model->user_id = \Yii::$app->user->identity->id;
            $model->customers_firstname = $profile->name;
            $model->customers_lastname = $profile->last_name;
            $model->customers_email = $profile->public_email;
            $model->customers_contact_phone = $profile->contact_phone;
            $model->customers_mobile_phone = $profile->mobile_phone;
            $model->order_city = $profile->city;
            $model->order_street = $profile->street;
            $model->order_house = $profile->house;
            $model->order_apartment = $profile->apartament;
            $model->order_location = $profile->location;
        }

        $model->items= $this->setItems(\Yii::$app->request->get('id'), \Yii::$app->request->get('amount') );
        $model->due_amount = $this->setPrice(\Yii::$app->request->get('id'), \Yii::$app->request->get('amount') );

        if( \Yii::$app->request->get('delivery') ){
            $model->delivery_method = \Yii::$app->request->get('delivery');
        }          

    

        if( \Yii::$app->request->get('payment') ){
            $model->payment_method = \Yii::$app->request->get('payment');
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['overview', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    public function setPrice($id, $amount = 1){
        if($id){
            $model = Product::findOne($id);
            return $model->price * $amount;    
        } else {
            return \Yii::$app->cart->getCost();
        }
    }    

    public function setItems($id, $amount = 1){
        if($id){
            return \Yii::$app->request->get('id').'-'.$amount;   
        } else {
            $items = [];
            foreach(\Yii::$app->cart->positions as $position){
                $items[] = $position->id.'-'.$position->quantity;
            }
            return implode(';', $items);
        }
    }



    /**
     * Updates an existing Order model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Order model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }    

    public function actionOrders()
    {
        if(\Yii::$app->user->isGuest){
            $this->goBack();
        }
        
        $query = Order::find()->where([
            'user_id' => \Yii::$app->user->identity->getId(),
        ]); 

        $dataProvider = new \yii\data\ActiveDataProvider([
          'query'=>$query,
        ]);

        return $this->render('orders', [
                'dataProvider'=>$dataProvider,
        ]);
    }

    /**
     * Finds the Order model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Order the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Order::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
