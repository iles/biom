<?php use yii\helpers\Html; ?>
<?php if($model->status < 3 ) : ?>
<div class="order-single">
    <p class="date"><?= date('d.m.Y', $model->created_at); ?></p>
    <div class="order-bar">
        <dl class="serial">
        <dt class="rp">Серийный номер</dt>
        <dd class="rp">BIOMN<?= str_pad($model->id, 7, '0', STR_PAD_LEFT); ?></dd>
        </dl>                        
        <dl class="status">
        <dt class="rp">Статус заказа</dt>
        <dd class="rp"><img src="img/green-bar.png">
            <?php 
                if($model->status == 1){
                    echo 'Ожидает обработки';
                } elseif ($model->status == 2) {
                    echo 'Ожидает доставки';
                }
            ?>
        </dd>
        </dl>
        <?= Html::a('Подробнее', ['order/view', 'id'=>$model->id], ['class'=>'waves-effect waves-green btn rp']) ;?>
        <div class="clearfix"></div>
    </div>
</div> 
<?php endif; ?>