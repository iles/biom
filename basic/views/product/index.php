<?php
    use yii\widgets\ListView;
?> 
        <div class="prod-part">
            <div class="head">
                <img src="img/prod-icon1.png">
                <div class="info">
                    <p class="sub-category rp">Говядина</p>
                    <p class="category rp">Мясная продукция</p>
                </div>
                <div class="main-head">
                    <a href="#">Войти</a>
                    <a href="#">Регистрация</a>
                </div>
                <div class="arrows">
                    <div class="left">
                        &#9668;
                    </div>
                    <div class="right">
                        &#9658;
                    </div>                   
                    
                </div>
            </div>
            <div class="main-prod main-section">

                <section class="basket-block">
                    <div class="top">
                        <img src="img/basket-logo-small.png">
                        <span class="rp">Ваша корзина</span>
                    </div>
                    <div class="basket-list">                         
                    <?php
                            /** @var ShoppingCart $sc */
                            foreach(\Yii::$app->cart->positions as $position){
                              echo $this->render('_cart_item',['position'=>$position]);
                              //var_dump($position);
                            }
                    ?>
                    </div>
                    <div class="bottom">
                        <p class="total rp">Итого на сумму</p>
                        <p class="price rp"><?= \Yii::$app->cart->getCost(); ?> тг</p>
                        <p class="total rp">Способ доставки</p>
                        <a href="#" class="m_button rp">Оформить заказ</a>
                    </div>
                </section>

                <?= ListView::widget( [
                        'dataProvider' => $dataProvider,
                        'itemView' => '_item',
                     ] ); 
                ?>

               

        </div>