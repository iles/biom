<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\searchOrder */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Orders';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Order', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'status',
            'serial_number',
            'user_id',
            'customers_firstname',
            // 'customers_lastname',
            // 'customers_email:email',
            // 'customers_contact_phone',
            // 'customers_mobile_phone',
            // 'order_city',
            // 'order_street',
            // 'order_house',
            // 'order_apartment',
            // 'order_location',
            // 'notyfication_method',
            // 'payment_method',
            // 'delivery_method',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

</div>
