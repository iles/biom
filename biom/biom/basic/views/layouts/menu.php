        <nav>
            <a href="#"><img src="img/main-logo.jpg"></a>
            <div class="sub-menu">
                <div class="menu" id="main">
                    <p class="title rp">Общее меню</p>
                    <ul>
                        <li><a href="#" class="rp">- О проекте</a></li>
                        <li><a href="#" class="rp">- Доставка и оплата</a></li>
                        <li><a href="#" class="rp">- Как сделать заказ</a></li>
                        <li><a href="#" class="rp">- Вопрос-ответ</a></li>
                        <li><a href="#" class="rp">- Как нас найти</a></li>
                    </ul>
                </div>                
                <div class="menu" id="meat">
                    <p class="title rp">Мясная продукция</p>
                    <ul>
                        <li><a href="#" class="rp">- Говядина</a></li>
                        <li><a href="#" class="rp">- Баранина</a></li>
                        <li><a href="#" class="rp">- Конина</a></li>
                        <li><a href="#" class="ep">- Свинина</a></li>
                    </ul>
                </div>                
                <div class="menu" id="fish">
                    <p class="title rp">Рыбная продукция</p>
                    <ul>
                        <li><a href="#" class="rp">- Лещ</a></li>
                        <li><a href="#" class="rp">- Форель</a></li>
                        <li><a href="#" class="rp">- Угорь</a></li>
                        <li><a href="#" class="rp">- Селедка</a></li>
                    </ul>
                </div>
            </div>
            <ul class="main-menu">
                <li class="submenu" menu="main">
                    <a href="#" class="main-menu">Меню</a>
                </li>
                <li class="nomenu">
                    
                </li>
                <li class="submenu" menu="meat"></li>
                <li class="submenu" menu="fish"></li>
                <li class="nomenu"></li>
                <li class="nomenu"></li>
                <li class="nomenu"></li>
                <li class="nomenu"></li>
            </ul>

            <p class="copyright">
                <span>&copy; 2015</span> bio-m.kz <br/>
                <span>Разработка</span> <a href="http://dynamica.kz">Dynamica</a>
            </p>
        </nav>