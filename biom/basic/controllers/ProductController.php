<?php

namespace app\controllers;

use app\models\Product;
use yii\data\ActiveDataProvider;
use yz\shoppingcart\ShoppingCart;

class ProductController extends \yii\web\Controller
{
    public function actionIndex()
    {
    	if (\Yii::$app->request->isAjax) {
    		$cart = new ShoppingCart();
    		$model = Product::findOne(\Yii::$app->request->get('id'));

    		if ($model) {
	        	\Yii::$app->cart->put($model, \Yii::$app->request->get('amount'));

                \Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
                $items = ['items' => $this->renderPartial('cart_block'), 'total' => \Yii::$app->cart->getCost()];
                return $items;
	    	}
 
	    	return 'wa';
    	}
        $searchModel = new Product();
        $parameters = \Yii::$app->request->queryParams;
		unset($parameters['r']);

		$query = Product::find()->where($parameters);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    public function actionRemove()
	{
        $cart = new ShoppingCart();
        $model = Product::findOne(\Yii::$app->request->get('id'));

        if ($model) {
            \Yii::$app->cart->remove($model);
            return \Yii::$app->cart->getCost();
        }
	}

}
