<?php

namespace app\models;


use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $status
 * @property integer $serial_number
 * @property integer $user_id
 * @property string $customers_firstname
 * @property string $customers_lastname
 * @property string $customers_email
 * @property string $customers_contact_phone
 * @property string $customers_mobile_phone
 * @property string $order_city
 * @property string $order_street
 * @property string $order_house
 * @property string $order_apartment
 * @property string $order_location
 * @property integer $notyfication_method
 * @property integer $payment_method
 * @property integer $delivery_method
 */
class Order extends \yii\db\ActiveRecord
{
    const STATUS_PROCESSING = 1;
    const STATUS_DELIVERY = 2;
    const STATUS_CLOSED = 3;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customers_firstname', 'customers_lastname', 'customers_email', 'customers_contact_phone', 'customers_mobile_phone', 'order_city', 'order_street', 'order_house', 'order_apartment',  'notyfication_method', 'payment_method', 'delivery_method'], 'required'],
            [['status', 'serial_number', 'due_amount', 'user_id', 'notyfication_method', 'payment_method', 'delivery_method'], 'integer'],
            [['customers_firstname', 'customers_lastname', 'customers_email', 'customers_contact_phone', 'customers_mobile_phone', 'order_city', 'order_street', 'order_house', 'order_apartment'], 'string', 'max' => 50],
            [['order_location'], 'string', 'max' => 70],
            [['items'], 'string', 'max' => 200],
        ];
    }

    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'Статус',
            'serial_number' => 'Серийный номер',
            'user_id' => 'User ID',
            'items' => 'Items',
            'due_amount' => 'Сумма заказа',
            'customers_firstname' => 'Имя',
            'customers_lastname' => 'Фамилия',
            'customers_email' => 'Email',
            'customers_contact_phone' => 'Контактный телефон',
            'customers_mobile_phone' => 'Мобильный телефон',
            'order_city' => 'Order City',
            'order_street' => 'Order Street',
            'order_house' => 'Order House',
            'order_apartment' => 'Order Apartment',
            'order_location' => 'Order Location',
            'notyfication_method' => 'Notyfication Method',
            'payment_method' => 'Payment Method',
            'delivery_method' => 'Delivery Method',
            'created_at' => 'Создан',
        ];
    }
}
