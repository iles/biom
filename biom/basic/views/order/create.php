<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
$this->title = 'Create Order';
if($model->order_location){
    $coord = explode(',', $model->order_location);
    $loc = "lat:".$coord[0].", lng:".$coord[1];
} else {
    $loc = "";
}
$this->title = Yii::t('user', 'Profile settings');
$this->params['breadcrumbs'][] = $this->title;

$this->registerJsFile('https://maps.googleapis.com/maps/api/js?callback=initMap', ['position' => \yii\web\View::POS_END ]);
$this->registerJs("       
var map;
var loc = {".$loc."};
var marker;
function initMap() {
  map = new google.maps.Map(document.getElementById('order-map'), {
    center: {lat: 43.221690, lng: 76.856403},
    zoom: 13
  });
    google.maps.event.addListener(map, 'click', function(event)
    {
        placeMarker(event.latLng);
        document.getElementById('order-order_location').value = event.latLng.lat() + ',' + event.latLng.lng();
    });

    if(loc){
        placeMarker(loc);
        map.setCenter(loc);
    }
}
    function placeMarker(location) {

            if(marker){
                marker.setMap(null);   
            }

            marker = new google.maps.Marker({
                position: location, 
                map: map
            });
             marker.setMap(map);

        };
", \yii\web\View::POS_END );
?>
        <div class="prod-part">
            <div class="head">
                <img src="img/basket-logo-big.png">
                <div class="info">
                    <p class="sub-category rp basket">Ваша корзина</p>
                </div>
            <div class="main-head">
            <?php if (!\Yii::$app->user->isGuest)  :?>
                <?= Html::a('Профиль', Url::toRoute('user/settings/profile'))?>
                <?= Html::a('Выйти', Url::toRoute('user/security/logout'), ['data-method' => 'post'])?>                
            <?php else : ?>
                <a href="#log_modal" class="modal-trigger">Войти</a>
                <a href="#reg_modal" class="modal-trigger">Регистрация</a>
            <?php endif; ?>
            </div>
                <div class="arrows">
                    <div class="left step rp">
                        1 шаг
                    </div>
                    <div class="right step active rp">
                        2 шаг
                    </div>                   
                    
                </div>
            </div>
            <div class="main-prod main-section">
            	<?php $form = ActiveForm::begin(['fieldConfig'=>['template'=>'{input}']]); ?>

                <section class="basket-block main-basket">
                    <div class="top">

                        <p class="rp">Общая сумма заказа</p>
                        <span class="rp"><?= $model->due_amount; ?> т</span>
                    </div>
                    <div class="basket-list">
                        <p class="bar">Способ оплаты</p>
                            <?= $form->field($model, 'payment_method', ['template'=>'{input}<label for="payment_1" class="rp">Наличными</label>'])->radio(['label' => null, 'value'=>1,  'id'=>'payment_1']); ?>
                            <?= $form->field($model, 'payment_method', ['template'=>'{input}<label for="payment_2" class="rp">Visa, MasterCard</label>'])->radio(['label' => null, 'value'=>2, 'id'=>'payment_2']); ?>
                      

                        <p class="bar">Способ доставки</p>
                        <?= $form->field($model, 'delivery_method', ['template'=>'{input}<label for="delivery_1" class="rp">City 1000</label>'])->radio(['label' => null, 'value'=>1,  'id'=>'delivery_1']); ?>
                        <?= $form->field($model, 'delivery_method', ['template'=>'{input}<label for="delivery_2" class="rp">Express</label>'])->radio(['label' => null, 'value'=>2,  'id'=>'delivery_2']); ?>
                          <p class="delivery">Тарифы <a href="#">Доставки</a></p>
                         <?= Html::submitButton('Далее', ['class' => 'm_button rp']) ?>
                        <input type="checkbox" name="agree" id="agree" value='1'>
                        <label for="agree">Полнстью согласен с <a href="#">Пользовательским соглашением</a></label>
                    </div>
                </section>

                <div class="basket-bar rp">
                    <span>Личные данные заказчика</span>
                </div>


               <div class="form-part">
               	    
                        <div class="form-bar rp">
                            Контактные данные
                            <p><?= $form->errorSummary($model); ?></p>
                        </div>
							<?= $form->field($model, 'customers_firstname')->textInput(['placeholder'=>'Имя', 'maxlength' => true]) ?>
							<?= $form->field($model, 'customers_lastname')->textInput(['placeholder'=>'Фамилия', 'maxlength' => true]) ?>
							<?= $form->field($model, 'customers_email')->textInput(['placeholder'=>'Ваш e-mail','maxlength' => true]) ?>
							<?= $form->field($model, 'customers_contact_phone')->textInput(['placeholder'=>'Контактный телефон', 'maxlength' => true]) ?>
							<?= $form->field($model, 'customers_mobile_phone')->textInput(['placeholder'=>'Мобильный телефон', 'maxlength' => true]) ?>
                        <div class="form-bar second rp">
                            Адрес доставки
                        </div>
							<?= $form->field($model, 'order_city')->textInput(['placeholder'=>'Город', 'maxlength' => true]) ?>
							<?= $form->field($model, 'order_street')->textInput(['placeholder'=>'Улица', 'maxlength' => true]) ?>
							<?= $form->field($model, 'order_house')->textInput(['placeholder'=>'Дом, строение', 'maxlength' => true]) ?>
							<?= $form->field($model, 'order_apartment')->textInput(['placeholder'=>'Квартира, офис', 'maxlength' => true]) ?>
                            <?= $form->field($model, 'order_location')->hiddenInput()->label(false); ?>

                         <a href="#" id="mapping" class="rp">Закрепить на карте</a>

                         <?= $form->field($model, 'notyfication_method', ['template'=>'{input}<label for="notify_1" class="rp">E-mail уведомление</label>'])->radio(['label' => null, 'value'=>1,  'id'=>'notify_1']); ?>
                         <?= $form->field($model, 'notyfication_method', ['template'=>'{input}<label for="notify_2" class="rp">СМС уведомление</label>'])->radio(['label' => null, 'value'=>2,  'id'=>'notify_2']); ?>

                </div>

                <div id="order-map"></div>

              
    <?php ActiveForm::end(); ?>
        </div>