<?php

/* @var $this yii\web\View */
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = 'My Yii Application';
?>
        <div class="main-part main-section">
            <div class="main-head">
            <?php if (!\Yii::$app->user->isGuest)  :?>
                <?= Html::a('Профиль', Url::toRoute('user/settings/profile'))?>
                <?= Html::a('Выйти', Url::toRoute('user/security/logout'), ['data-method' => 'post'])?>                
            <?php else : ?>
                <a href="#log_modal" class="modal-trigger">Войти</a>
                <a href="#reg_modal" class="modal-trigger">Регистрация</a>
            <?php endif; ?>
            </div>
            <p class="header rp">Дары Заилийского Алатау</p>
            <img src="img/main-block.png"/>
            <div class="main-buttons">
                <?= Html::a('Начать покупку', ['product/index'], ['class'=>'waves-effect waves-green btn rp']); ?>
                <?php if (\Yii::$app->user->isGuest)  :?>
                    <a href="#reg_modal" class="modal-trigger waves-effect waves-green btn rp ">Регистрация</a>
                <?php endif;  ?>
            </div>
        </div>
