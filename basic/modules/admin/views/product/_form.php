<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Category;
/* @var $this yii\web\View */
/* @var $model app\models\product */
/* @var $form yii\widgets\ActiveForm */
?>

  <div class="row">
<?php $form = ActiveForm::begin([
            'options' => ['enctype' => 'multipart/form-data'],
            'fieldConfig' => [
                'template' =>
                     "{input}{label}{error}",

            ]
]); ?>
    <div class="col s12">

      <div class='row'>
        <?= $form->errorSummary($model); ?>
      </div>
      <div class="row">
        <div class="input-field col s3">

      <?= Html::activeDropDownList($model, 'category',
      ArrayHelper::map(Category::find()->all(), 'id', 'name')) ?>


       </div>
        <div class="input-field col s3">
        <?= $form->field($model, 'sub_category')->dropDownList(
                ['1'=>'1', '2'=>'2','3'=>'3', '4'=>'4','5'=>'5', '6'=>'6'],           // Flat array ('id'=>'label')
            ['prompt'=>'Выберите категорию']    // options
        ); 
        ?>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
         <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
        <?= $form->field($model, 'price')->textInput() ?>
        </div>
      </div>
      <div class="row">
        <div class="input-field col s12">
    <?= $form->field($model, 'desc')->textArea(['maxlength' => true]) ?>
        </div>
      </div>      
      <div class="row">
        <div class="input-field col s12">
    <?= $form->field($model, 'image[]',[
  'template' => '    <div class="file-field input-field">
      <div class="btn">
        <span>Файл</span>
        {input}
      </div>
      <input class="file-path validate" disabled type="text"/>
    </div>'
])->fileInput(['multiple' => true, 'accept' => 'image/*']); ?>

        </div>
      </div>




    <div class="form-group sub-button">
        <?= Html::submitButton($model->isNewRecord ? 'Создать' : 'Update', ['class' => $model->isNewRecord ? 'waves-effect waves-light btn-large  deep-orange' : 'btn btn-primary']) ?>
    </div>

    </div>
        <?php ActiveForm::end(); ?>
  </div>

