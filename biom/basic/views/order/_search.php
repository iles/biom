<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\searchOrder */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'status') ?>

    <?= $form->field($model, 'serial_number') ?>

    <?= $form->field($model, 'user_id') ?>

    <?= $form->field($model, 'customers_firstname') ?>

    <?php // echo $form->field($model, 'customers_lastname') ?>

    <?php // echo $form->field($model, 'customers_email') ?>

    <?php // echo $form->field($model, 'customers_contact_phone') ?>

    <?php // echo $form->field($model, 'customers_mobile_phone') ?>

    <?php // echo $form->field($model, 'order_city') ?>

    <?php // echo $form->field($model, 'order_street') ?>

    <?php // echo $form->field($model, 'order_house') ?>

    <?php // echo $form->field($model, 'order_apartment') ?>

    <?php // echo $form->field($model, 'order_location') ?>

    <?php // echo $form->field($model, 'notyfication_method') ?>

    <?php // echo $form->field($model, 'payment_method') ?>

    <?php // echo $form->field($model, 'delivery_method') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
