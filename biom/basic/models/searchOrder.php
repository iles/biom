<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Order;

/**
 * searchOrder represents the model behind the search form about `app\models\Order`.
 */
class searchOrder extends Order
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'status', 'serial_number', 'user_id', 'notyfication_method', 'payment_method', 'delivery_method'], 'integer'],
            [['customers_firstname', 'customers_lastname', 'customers_email', 'customers_contact_phone', 'customers_mobile_phone', 'order_city', 'order_street', 'order_house', 'order_apartment', 'order_location'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Order::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
            'serial_number' => $this->serial_number,
            'user_id' => $this->user_id,
            'notyfication_method' => $this->notyfication_method,
            'payment_method' => $this->payment_method,
            'delivery_method' => $this->delivery_method,
        ]);

        $query->andFilterWhere(['like', 'customers_firstname', $this->customers_firstname])
            ->andFilterWhere(['like', 'customers_lastname', $this->customers_lastname])
            ->andFilterWhere(['like', 'customers_email', $this->customers_email])
            ->andFilterWhere(['like', 'customers_contact_phone', $this->customers_contact_phone])
            ->andFilterWhere(['like', 'customers_mobile_phone', $this->customers_mobile_phone])
            ->andFilterWhere(['like', 'order_city', $this->order_city])
            ->andFilterWhere(['like', 'order_street', $this->order_street])
            ->andFilterWhere(['like', 'order_house', $this->order_house])
            ->andFilterWhere(['like', 'order_apartment', $this->order_apartment])
            ->andFilterWhere(['like', 'order_location', $this->order_location]);

        return $dataProvider;
    }
}
