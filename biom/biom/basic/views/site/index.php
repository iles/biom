<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
        <div class="main-part main-section">
            <div class="main-head">
                <a href="#">Войти</a>
                <a href="#">Регистрация</a>
            </div>
            <p class="header rp">Дары Заилийского Алатау</p>
            <img src="img/main-block.png"/>
            <div class="main-buttons">
                <a href="#" class="waves-effect waves-green btn rp">Начать покупку</a>
                <a href="#reg_modal" class="modal-trigger waves-effect waves-green btn rp ">Регистрация</a>
            </div>
        </div>
