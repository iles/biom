<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Order */
/* @var $form yii\widgets\ActiveForm */ 
?>

<div class="order-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <?= $form->field($model, 'serial_number')->textInput() ?>

    <?= $form->field($model, 'user_id')->textInput() ?>

    <?= $form->field($model, 'customers_firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customers_lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customers_email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customers_contact_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'customers_mobile_phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'order_city')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'order_street')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'order_house')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'order_apartment')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'order_location')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'notyfication_method')->textInput() ?>

    <?= $form->field($model, 'payment_method')->textInput() ?>

    <?= $form->field($model, 'delivery_method')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
