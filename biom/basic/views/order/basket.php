<?php
    use yii\widgets\ListView;
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;
?> 
      <div class="prod-part">
            <div class="head">
                <img src="img/basket-logo-big.png">
                <div class="info">
                    <p class="sub-category rp basket">Ваша корзина</p>
                </div>
            <div class="main-head">
            <?php if (!\Yii::$app->user->isGuest)  :?>
                <?= Html::a('Профиль', Url::toRoute('user/settings/profile'))?>
                <?= Html::a('Выйти', Url::toRoute('user/security/logout'), ['data-method' => 'post'])?>                
            <?php else : ?>
                <a href="#log_modal" class="modal-trigger">Войти</a>
                <a href="#reg_modal" class="modal-trigger">Регистрация</a>
            <?php endif; ?>
            </div>  
                <div class="arrows">
                    <div class="left step active rp">
                        1 шаг
                    </div>
                    <div class="right step rp">
                        2 шаг
                    </div>                   
                    
                </div>
            </div>
            <div class="main-prod main-section">

                <section class="basket-block main-basket">
                    <div class="top">

                        <p class="rp">Общая сумма заказа</p>
                        <span class="rp"><?= \Yii::$app->cart->getCost(); ?> т</span>
                    </div>
                    <div class="basket-list">
                            <?php $form = ActiveForm::begin([
                                'method'=>'get',
                                'action'=> Url::toRoute('order/create'),
                            ]); ?>
                            <p class="bar">Способ оплаты</p>
                             <div class="input-wrap">
                                <input type="radio" id="pay_1" checked="true" name="payment" value="1">
                                <label for="pay_1" class="rp black_label" >Наличными</label>    
                            </div>                            
                            <div class="input-wrap">
                                <input type="radio" id="pay_2" name="payment" value="2">
                                <label for="pay_2" class="rp black_label">Visa, MasterCard</label>   
                            </div>    
                            <p class="bar">Способ доставки</p>                           
                            <div class="input-wrap">
                                <input type="radio" id="del_1" checked="true" name="delivery" value="1">
                                <label for="del_1" class="rp black_label" >City 1000</label>    
                            </div>                            
                            <div class="input-wrap">
                                <input type="radio" id="del_2" name="delivery" value="2">
                                <label for="del_2" class="rp black_label">Express</label>   
                            </div>       
                <?= Html::submitButton('Оформить заказ', ['class' => 'm_button rp']) ?>  
                    </div>
                </section>

                <div class="basket-bar rp">
                    <span>Список выбранных товаров</span>
                    <p><?= \Yii::$app->cart->getCount(); ?></p>
                </div>

                <div class="items-list">

                	<?php
                            /** @var ShoppingCart $sc */
                            foreach(\Yii::$app->cart->positions as $position){
                              echo $this->render('_cart_item',['position'=>$position]);
                              //var_dump($position);
                            }
                    ?>
                </div>

              

        </div>