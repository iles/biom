<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=bm_db',
    'username' => 'homestead',
    'password' => 'secret',
    'charset' => 'utf8',
];
